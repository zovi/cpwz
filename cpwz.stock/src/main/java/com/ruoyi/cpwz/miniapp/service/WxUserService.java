package com.ruoyi.cpwz.miniapp.service;

import cn.binarywang.wx.miniapp.bean.WxMaUserInfo;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.shiro.service.SysPasswordService;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.service.ISysUserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WxUserService {

    @Autowired
    private ISysUserService userService;
    @Autowired
    private SysPasswordService passwordService;

    public Object LoginAndRegesit(WxMaUserInfo userInfo) {
        SysUser user = new SysUser();
        user.setLoginName(userInfo.getNickName());
        //判断用户的电话号码数据库是否已存在，不存在则新增用户，存在则直接登录
        if(UserConstants.USER_NAME_UNIQUE.equals(userService.checkLoginNameUnique(userInfo.getNickName()))){
            //开始放信息
            user.setAvatar(userInfo.getAvatarUrl());
            //微信登录所有密码都设置为默认123456
            user.setPassword("123456");
            user.setSalt(ShiroUtils.randomSalt());
            user.setPassword(passwordService.encryptPassword(user.getLoginName(), user.getPassword(), user.getSalt()));
            user.setCreateBy("微信登录");
            userService.insertUser(user);
        }
//        //开始登录
        UsernamePasswordToken token = new UsernamePasswordToken(user.getLoginName(), user.getPassword(), false);
        Subject subject = SecurityUtils.getSubject();
        try
        {
            subject.login(token);
            return token;
        }
        catch (AuthenticationException e)
        {
            String msg = "用户或密码错误";
            if (StringUtils.isNotEmpty(e.getMessage()))
            {
                msg = e.getMessage();
            }
            return msg;
        }

    }
}
