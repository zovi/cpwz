drop table if exists chen_material_info;
create table chen_material_info
(
    material_id    int auto_increment comment 'id'
        primary key,
    material_code  varchar(30)            null comment '商品编码',
    material_name  varchar(30) default '' null comment '商品名称',
    type_id        int(20)                null comment '商品类型(type表)',
    material_model varchar(32) default '' null comment '规格型号',
    remark         varchar(200)           null comment '备注',
    creat_time     datetime               null comment '创建时间',
    unit           char(30)               null comment '计量单位',
    stock          char(30)               null comment '仓库',
    UNIQUE KEY `we` (`material_name`,`material_model`) USING BTREE
) engine = innodb
  auto_increment = 1 comment = '商品信息表';
drop table if exists chen_material_in_main;
create table chen_material_in_main
(
    in_main_id   int auto_increment comment 'id'
        primary key,
    in_main_code varchar(30) null comment '入库单编码',
    create_time  datetime    null comment '创建时间',
    carriage     decimal(24) null comment '运费',
    tax_rate     char        null comment '税率',
    tax_price    decimal(24) null comment '税费',
    total_price  decimal(24) null comment '合计'
) engine = innodb
  auto_increment = 1 comment = '入库主表';
drop table if exists chen_material_out_main;
create table chen_material_out_main
(
    out_main_id   int auto_increment comment 'id'
        primary key,
    out_main_code varchar(30) null comment '出库单编码',
    create_time   datetime    null comment '创建时间',
    carriage      decimal(24) null comment '运费',
    total_price   decimal(24) null comment '合计价格（毛）',
    total_profit  decimal(24) null comment '本单利润'
) engine = innodb
  auto_increment = 1 comment = '出库主表';
drop table if exists chen_material_out_detail;
create table chen_material_out_detail
(
    out_detail_id int auto_increment comment 'id'
        primary key,
    material_id   int         null comment '商品名称',
    out_main_code varchar(30) null comment '出库单编码',
    stock_id      int         null comment '仓库',
    create_time   datetime    null comment '创建时间',
    out_num       int(20)     null comment '出库数量',
    price         decimal(24) null comment '单价',
    total_price   decimal(24) null comment '总价',
    carriage      decimal(24) null comment '运费',
    unit          char        null comment '计量单位'
) engine = innodb
  auto_increment = 1 comment = '出库从表';
drop table if exists chen_material_in_detail;
create table chen_material_in_detail
(
    in_detail_id int auto_increment comment 'id'
        primary key,
    material_id  int         null comment '商品名称',
    in_main_code varchar(30) null comment '入库单编码',
    stock_id     int         null comment '仓库',
    create_time  datetime    null comment '创建时间',
    out_num      int(20)     null comment '出库数量',
    price        decimal(24) null comment '单价',
    total_price  decimal(24) null comment '总价',
    tax_price    decimal(24) null comment '税费',
    carriage     decimal(24) null comment '运费',
    unit         char        null comment '计量单位'
) engine = innodb
  auto_increment = 1 comment = '入库从表';
drop table if exists chen_stock_info;
create table chen_stock_info
(
    info_id      int auto_increment comment 'id'
        primary key,
    material_id  int         null comment '商品名称',
    stock_id     int         null comment '仓库',
    create_time  datetime    null comment '创建时间',
    stock_number int(20)     null comment '库存数量',
    avg_price    decimal(24) null comment '平均单价',
    total_price  decimal(24) null comment '库存总价',
    low_price    decimal(24) null comment '最低售价',
    carriage     decimal(24) null comment '运费',
    unit         char        null comment '计量单位'
) engine = innodb
  auto_increment = 1 comment = '库存信息表';
drop table if exists chen_material_type;
create table chen_material_type
(
    type_id   bigint auto_increment comment '产品id'
        primary key,
    parent_id bigint      default 0   null comment '父产品id',
    type_name varchar(30) default ''  null comment '产品名称',
    remark    varchar(32) default ''  null comment '备注',
    status    char        default '0' null comment '产品状态（0正常 1停用）'
)engine = innodb
 auto_increment = 1 comment '商品分类表';

